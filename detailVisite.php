<?php

include("_debut.inc.php");

$idVisite=$_REQUEST['idVisite'];

// OBTENIR LE DÉTAIL DE L'ÉTABLISSEMENT SÉLECTIONNÉ

$req="select * from visite, entreprise, activite
		where entreprise.idActivite=activite.id and visite.idEntreprise = entreprise.id
		and visite.id=$idVisite";
$rsVisite = $conbd->query($req);
$lgVisite =  $rsVisite->fetch();

$adresse=$lgVisite['adresse'];
$ville=$lgVisite['ville'];
$activite=$lgVisite['libelle'];
$date = dateAnglaisVersFrancais($lgVisite['dateV']);
$heureDebut=$lgVisite['heureDebut'];
$nbPlacesMax=$lgVisite['nbPlacesMax'];
$nbPlacesMin=$lgVisite['nbPlacesMin'];
$description=$lgVisite['description'];
$nbVisiteursInscrits=$lgVisite['nbVisiteursInscrits'];
$nomEntreprise=$lgVisite['raisonSociale'];
$nomContact=$lgVisite['nomContact'];
$telContact=$lgVisite['telContact'];
?>
<table width='60%' cellspacing='0' cellpadding='0' align='center'
		class='tabNonQuadrille'>
   <tr class='ligneTabNonQuad'>
      <td  width='20%'> Entreprise: </td>
      <td><?php echo $nomEntreprise; ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
         <td  width='20%'> Activité: </td>
         <td><?php echo $activite;  ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
      <td> Adresse: </td>
      <td><?php echo $adresse;  ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
      <td> Ville: </td>
      <td><?php echo $ville;  ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
      <td> Jour: </td>
      <td><?php echo $date;  ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
      <td> heure: </td>
      <td><?php echo $heureDebut;  ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
         <td> Places reservées: </td>
         <td><?php echo $nbVisiteursInscrits;  ?></td>
   </tr>
   <tr class='ligneTabNonQuad'>
      <td> Nombre maximum de Places: </td>
      <td><?php echo $nbPlacesMax;  ?></td>
   </tr>
	<tr class='ligneTabNonQuad'>
	      <td> Nombre minimum de Places: </td>
	      <td><?php echo $nbPlacesMin;  ?></td>
   </tr>

   <tr class='ligneTabNonQuad'>
      <td> contact : </td>
      <td><?php echo $nomContact.' '.$telContact;  ?>
      </td>
   </tr>

</table>
<table align='center'>
   <tr>
      <td  align='center'><a href='listeVisitesPourDetail.php'>Retour liste des visites</a>
      </td>
      <td align='center'><a href='listeInscrits.php?idVisite=<?php echo $idVisite;?>'>Liste des inscrits</a>
      </td>
      <td align='center'><a href='annulerVisite.php?idVisite=<?php echo $idVisite;?>&action=demanderAnnulerVisite'>Annuler la visite</a>
      </td>
   </tr>
</table>"
