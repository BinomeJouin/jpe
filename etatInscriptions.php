<?php
define('FPDF_FONTPATH','fpdf/font/');
require('fpdf/fpdf.php');

include("_fonctions.inc.php");


// CONNEXION AU SERVEUR MYSQL PUIS S�LECTION DE LA BASE DE DONN�ES jpe

try{
$conbd=connect();
}
catch(Exception $e){
echo $e -> getMessage ();
exit();
}

$pdf=new FPDF();




$req = "select  visite.id as visiteId, visite.dateV, visite.heureDebut, visite.nbPlacesMin, visite.nbPlacesMax,
visite.nbVisiteursInscrits, entreprise.raisonSociale, entreprise.nomContact, entreprise.telContact from visite, entreprise
where entreprise.id = visite.idEntreprise
and visite.etat like 'ouverte' ";
   $rsVisite = $conbd->query($req);
   $lesVisites =  $rsVisite->fetchAll();
// Boucle de parcours des visites
   foreach ($lesVisites as $lgVisite)
{
	$pdf->SetFont('Arial','',12);
	$pdf->AddPage();		// une page par visite
	$idVisite= $lgVisite['visiteId'];
	$rais= $lgVisite['raisonSociale'];
	$nomContact =$lgVisite['nomContact'];
	$date = $lgVisite['dateV'];
	$date = dateAnglaisVersFrancais($date);
	$heure = $lgVisite['heureDebut'];
	$telContact = $lgVisite['telContact'];
	$enTete=array('Entreprise d\'accueil','date', 'Heure' ,' Nom du contact', 't�l�phone');
	for($i=0;$i<5;$i++)
		$pdf->Cell(38,8,$enTete[$i],1,0,'C');
	$valeurEnTete = array($rais,$date,$heure,$nomContact,$telContact);
	$pdf->Ln();
	for($i=0;$i<5;$i++)
		$pdf->Cell(38,16,$valeurEnTete[$i],1,0,'C');

	$pdf->Ln();
	$pdf->Ln();
	$nbPlacesMin = $lgVisite['nbPlacesMin'];
	$nbPlacesMax = $lgVisite['nbPlacesMax'];
	$nbPlacesUtilisees = $lgVisite['nbVisiteursInscrits'];
	$enTeteSituation= array('Nombre minimum de places','Nombre maximum de places','Nombre de places r�serv�es');
	for($i=0;$i<3;$i++)
		$pdf->Cell(55,8,$enTeteSituation[$i],1,0,'C');
	$pdf->Ln();
	$valeurEnTeteSituation = array($nbPlacesMin,$nbPlacesMax,$nbPlacesUtilisees);
	for($i=0;$i<3;$i++)
		$pdf->Cell(55,8,$valeurEnTeteSituation[$i],1,0,'C');
    $req = "select visiteur.nom, visiteur.prenom, visiteur.nbPersonnes From visiteur, visite
    where visite.id = visiteur.idVisite and visiteur.idVisite = $idVisite ";
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial','',10);
    //$data=$pdf->LoadRequete($connexion,$req);
    $header=array('Nom','Prenom','Nombre de personnes');
    $pdf->FancyTableEnTete($header);
    $rs = $conbd->query($req);
	$lesInsc = $rs->fetchAll();
	 foreach ($lesInsc as $unInsc)
	 {

	   for($i=0;$i<3;$i++)
		$pdf->Cell(40,8,$unInsc[$i],1,0,'C');
	 
	  $pdf->Ln();
	 }
}
$pdf->Output();
?>