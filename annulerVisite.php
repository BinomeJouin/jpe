<?php

include("_debut.inc.php");

// ANNULER UNE VISITE

// On récupère l'identifaint de visite du formulaire précédent
$idVisite=$_REQUEST['idVisite'];

// On récupère les informations de la visite pour les afficher
$req='select * from visite, entreprise, activite where entreprise.idActivite=activite.id
and visite.idEntreprise = entreprise.id and visite.id='.$idVisite;
$rsVisite = $conbd->query($req);
$lgVisite =  $rsVisite->fetch();

$date=$lgVisite['dateV'];
//Utilisation d'une fonction de conversion de date pour l'afficher au format français
$date = dateAnglaisVersFrancais($date);
$heureDebut=$lgVisite['heureDebut'];
$nomEntreprise=$lgVisite['raisonSociale'];


// Cas 1ère étape (on vient du formulaire detailVisite.php)

if ($_REQUEST['action']=='demanderAnnulerVisite')
{
   ?>
   <br/><center><h5>Souhaitez-vous vraiment annuler la visite du <?php echo $date;?> à <?php echo $nomEntreprise ;?> ?
   <br/><br/>
   <a href='annulerVisite.php?action=validerAnnulationVisite&idVisite=<?php echo $idVisite;?>'>
   Oui</a>&nbsp; &nbsp; &nbsp; &nbsp;
   <a href='detailVisite.php?idVisite=<?php echo $idVisite;?>'>Non</a></h5></center>
   <?php
}

// Cas 2ème étape (on vient de demander à annuler la visite en cliquant sur 'oui')

else
{
   // On modifie le champ 'etat' de la visite
   $req = "update visite set etat='fermee' where visite.id =$idVisite";
   $conbd->exec($req);
   ?>
   <br/><br/><center><h5>La visite du <?php echo $date;?> à <?php echo $nomEntreprise ;?> été annulée</h5>
   <br/><br/><center><h5><a href='listeVisiteursAPrevenir.php?idVisite=<?php echo $idVisite;?>'>Liste des inscrits à prévenir</a></h5>
   <a href='index.php?'>Retour</a></center>
<?php
}
?>