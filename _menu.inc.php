<!DOCTYPE HTML>
<!-- TITRE ET MENUS -->
<html lang="fr">
<head>
<title>JPE</title>
<meta charset="utf-8">
<link href="css/cssGeneral.css" rel="stylesheet" type="text/css">
</head>
<body class="basePage">

<!--  En-t�te -->
<img src="images/logo.png"	alt="Jeune Chambre Economique" title="JCE : Jeune Chambre Economique"/>
<img src="images/bandeau.png"	alt="Jeune Chambre Economique" title="JCE : Jeune Chambre Economique"/>

<!--  Menu gauche-->
<br/>
<div class="gauche">
	<ul class="menugauche">
		<li><a href="index.php">Accueil JPE 2008</a></li>
		<li><a href="listeVisitesPourDetail.php">Gestion des visites</a></li>
		<li><a href="listeVisitesPourInscription.php">Inscription visite</a></li>
		<li><a href="etatInscriptions.php">Edition des listes des inscrits</a></li>
	</ul>
</div>
